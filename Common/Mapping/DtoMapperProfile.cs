﻿using Common.Dtos;
using Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Mapping
{
    public class DtoMapperProfile: AutoMapper.Profile
    {
        public DtoMapperProfile()
        {
            CreateMap<User, UserDto>().PreserveReferences().MaxDepth(1);
            CreateMap<Project, ProjectDto>().PreserveReferences().MaxDepth(1);
            //CreateMap<Task, TaskDto>();
        }
    }
}
