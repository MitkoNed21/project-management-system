﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    public class Task
    {
        public enum TaskStatus
        {
            Pending,
            InProgress,
            InReview,
            Completed
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }
        [Required]
        public TaskStatus Status { get; set; }

        public int OwnerId { get; set; }
        [ForeignKey(nameof(OwnerId))]
        //[InverseProperty(nameof(User.CreatedTasks))]
        public virtual User Owner { get; set; }

        public int AssigneeId { get; set; }
        [ForeignKey(nameof(AssigneeId))]
        //[InverseProperty(nameof(User.AssignedTasks))]
        public virtual User Assignee { get; set; }

        public int ProjectId { get; set; }
        [ForeignKey(nameof(ProjectId))]
        public virtual Project Project { get; set; }
    }
}
