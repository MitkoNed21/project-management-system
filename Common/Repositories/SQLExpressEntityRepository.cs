﻿using Common.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectManagement.Migrations;

namespace Common.Repositories
{
    public class SQLExpressEntityRepository<TEntity> : IEntityRepository<TEntity> where TEntity : class
    {
        DbContext context;
        DbSet<TEntity> dbSet;

        public SQLExpressEntityRepository(DbContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }

        public TEntity? GetById(int id)
        {
            return dbSet.Find(id);
        }

        public TEntity? Get(Func<TEntity, bool> predicate)
        {
            return dbSet.FirstOrDefault(predicate);
        }

        public bool Exists(int id)
        {
            return dbSet.Find(id) is not null;
        }

        public bool Exists(Func<TEntity, bool> predicate)
        {
            return dbSet.Any(predicate);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return dbSet;
        }

        public IEnumerable<TEntity> GetAll(Func<TEntity, bool> predicate)
        {
            return dbSet.Where(predicate);
        }

        public TEntity Add(TEntity entity)
        {
            dbSet.Add(entity);
            context.SaveChanges();
            return entity;
        }

        public TEntity Update(TEntity entity)
        {
            //TODO: TEST!!!!!
            dbSet.Update(entity);
            //var dbUser = context.Users.Find(user.Id);
            //
            //dbUser.Username = user.Username;
            //dbUser.Password = user.Password;
            //dbUser.FirstName = user.FirstName;
            //dbUser.LastName = user.LastName;
            //
            //context.Update(dbUser);
            context.SaveChanges();

            return entity;
        }

        public TEntity? Delete(int id)
        {
            var entity = context.Find<TEntity>(id);

            if (entity is not null)
            {
                context.Remove(entity);
                context.SaveChanges();
            }

            return entity;
        }
    }
}
