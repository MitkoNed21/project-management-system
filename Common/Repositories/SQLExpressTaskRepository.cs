﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Repositories
{
    public class SQLExpressTaskRepository : SQLExpressEntityRepository<Entities.Task>
    {
        public SQLExpressTaskRepository(DbContext context) : base(context) { }

        public IQueryable<Entities.Task> Tasks => GetAll().AsQueryable();
    }
}
