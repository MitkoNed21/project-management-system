﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Common.Entities;

namespace Common.Repositories
{
    public class ProjectManagementSystemDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Entities.Task> Tasks { get; set; }

        public ProjectManagementSystemDbContext()
        {
            Users = Set<User>();
            Projects = Set<Project>();
            Tasks = Set<Entities.Task>();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                @"Server=localhost\SQLEXPRESS;Database=ProjectManagementSystemDB;Trusted_Connection=True;"
            );
            optionsBuilder.UseLazyLoadingProxies();
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var user = new User()
            {
                Id = 1,
                Username = "admin",
                Password = "admin",
                FirstName = "admin",
                LastName = "admin"
            };

            user.Password = new PasswordHasher<User>().HashPassword(user, user.Password);

            modelBuilder.Entity<User>().HasData(user);
            modelBuilder.Entity<User>().HasMany(u => u.Projects).WithMany(p => p.SharedUsers);
            modelBuilder.Entity<Project>().HasOne(p => p.Owner);

            base.OnModelCreating(modelBuilder);
        }
    }
}
