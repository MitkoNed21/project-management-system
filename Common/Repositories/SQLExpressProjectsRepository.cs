﻿using Common.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Repositories
{

    public class SQLExpressProjectsRepository : SQLExpressEntityRepository<Project>
    {
        public SQLExpressProjectsRepository(DbContext context) : base(context) { }

        public IQueryable<Project> Projects => GetAll().AsQueryable();
    }
}
