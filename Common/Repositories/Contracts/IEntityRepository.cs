﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Repositories.Contracts
{
    public interface IEntityRepository<TEntity>
    {
        public TEntity? GetById(int id);
        public TEntity? Get(Func<TEntity, bool> predicate);
        public bool Exists(int id);
        public bool Exists(Func<TEntity, bool> predicate);
        public IEnumerable<TEntity> GetAll();
        public IEnumerable<TEntity> GetAll(Func<TEntity, bool> predicate);

        public TEntity Add(TEntity entity);
        public TEntity Update(TEntity entity);
        public TEntity? Delete(int id);
    }
}
