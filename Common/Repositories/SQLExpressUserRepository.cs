﻿using Common.Entities;
using Common.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Common.Repositories
{
    public class SQLExpressUserRepository : SQLExpressEntityRepository<User>
    {
        public SQLExpressUserRepository(DbContext context) : base(context) { }

        public IQueryable<User> Users => GetAll().AsQueryable();
    }
}
