﻿namespace Common.Dtos
{
    public class UserDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }   
        public string LastName { get; set; }

        public virtual ICollection<ProjectDto> Projects { get; set; }
        //public virtual List<TaskDto> AssignedTasks { get; set; }
        //public virtual List<TaskDto> CreatedTasks { get; set; }
    }
}
