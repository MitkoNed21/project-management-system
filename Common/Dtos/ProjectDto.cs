﻿namespace Common.Dtos
{
    public class ProjectDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public virtual UserDto Owner { get; set; }
        //public virtual List<TaskDto> Tasks { get; set; }

        public List<UserDto> SharedUsers { get; set; }
    }
}
