﻿using ProjectManagement.Utilities;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace ProjectManagement.ValidationAttributes
{
    public class AllowedCharactersAttribute : ValidationAttribute
    {
        Regex validationCharsRegex;

        public AllowedCharactersAttribute(Regex validationCharsRegex)
        {
            this.validationCharsRegex = validationCharsRegex;
        }

        public AllowedCharactersAttribute(string validationCharsRegex)
        {
            this.validationCharsRegex = new Regex(validationCharsRegex);
        }

        public override bool RequiresValidationContext => false;

        public override bool IsValid(object? value)
        {
            var str = value as string;
            if (str is null) throw new ArgumentNullException(nameof(value));

            if (String.IsNullOrWhiteSpace(str))
            {
                throw new ArgumentNullException(
                    nameof(str),
                    "Input string is empty or consists only of white space characters!"
                );
            }

            var usernameValidCharsMatch = validationCharsRegex.Match(str);

            // returns true if there is a match, false otherwise
            return usernameValidCharsMatch.Success;
        }
    }
}
