﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ProjectManagement.ViewModels.Home
{
    public class LoginViewModel
    {
        [DisplayName("Username: ")]
        [Required(ErrorMessage = "This field is required!")]
        public string Username { get; set; }

        [DisplayName("Password: ")]
        [Required(ErrorMessage = "This field is required!")]
        public string Password { get; set; }
    }
}
