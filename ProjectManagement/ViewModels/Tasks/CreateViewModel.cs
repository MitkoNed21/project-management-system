﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ProjectManagement.ViewModels.Tasks
{
    public class CreateViewModel
    {
        [DisplayName("Title: ")]
        [Required(ErrorMessage = "This field is required!")]
        public string Title { get; set; }

        [DisplayName("Description: ")]
        public string Description { get; set; }
        
        [Required(ErrorMessage = "This field is required!")]
        [DisplayName("Assign to: ")]
        public int AssigneeId { get; set; }

        public int ProjectId { get; set; }
        public string ProjectTitle { get; set; }

        public List<CreateTaskUserViewModel> Users { get; set; } = new List<CreateTaskUserViewModel>();
    }
}
