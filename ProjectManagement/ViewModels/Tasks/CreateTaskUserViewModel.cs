﻿namespace ProjectManagement.ViewModels.Tasks
{
    public class CreateTaskUserViewModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
