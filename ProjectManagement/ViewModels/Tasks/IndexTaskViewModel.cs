﻿namespace ProjectManagement.ViewModels.Tasks
{
    public class IndexTaskViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public TaskStatus Status { get; set; }

        public string AssigneeUsername { get; set; }
    }
}