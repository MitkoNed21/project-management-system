﻿namespace ProjectManagement.ViewModels.Tasks
{
    public class IndexViewModel
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public List<IndexTaskViewModel> Tasks { get; set; } = new List<IndexTaskViewModel>();

    }
}