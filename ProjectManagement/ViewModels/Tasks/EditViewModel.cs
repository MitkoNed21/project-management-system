﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ProjectManagement.ViewModels.Tasks
{
    public class EditViewModel
    {
        [DisplayName("Title: ")]
        [Required(ErrorMessage = "This field is required!")]
        public string Title { get; set; } 

        [DisplayName("Description: ")]
        public string Description { get; set; }

        [DisplayName("Status: ")]
        [Required(ErrorMessage = "This field is required!")]
        public TaskStatus Status { get; set; }

        [DisplayName("Assigned to: ")]
        [Required(ErrorMessage = "This field is required!")]
        public int AssigneeId { get; set; }

        public int ProjectId { get; set; }
    }
}
