﻿using ProjectManagement.Utilities;
using ProjectManagement.ValidationAttributes;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ProjectManagement.ViewModels.Projects
{
    public class CreateViewModel
    {
        [DisplayName("Title: ")]
        [Required(ErrorMessage = "This field is required!")]
        public string Title { get; set; }

        [DisplayName("Description: ")]
        public string Description { get; set; }
    }
}
