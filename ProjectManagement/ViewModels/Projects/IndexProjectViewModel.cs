﻿namespace ProjectManagement.ViewModels.Projects
{
    public class IndexProjectViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string OwnerUsername { get; set; }
    }
}
