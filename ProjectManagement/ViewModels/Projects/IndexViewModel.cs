﻿namespace ProjectManagement.ViewModels.Projects
{
    public class IndexViewModel
    {
        public List<IndexProjectViewModel> Projects { get; set; } = new List<IndexProjectViewModel>();
    }
}
