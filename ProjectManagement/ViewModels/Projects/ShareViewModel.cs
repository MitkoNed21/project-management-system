﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ProjectManagement.ViewModels.Projects
{
    public class ShareViewModel
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }

        [DisplayName("Share with: ")]
        [Required(ErrorMessage = "Please select a user.")]
        public int[] UserIdsToShareWith { get; set; }

        public List<ShareProjectUserViewModel> SharedUsers { get; set; } = new List<ShareProjectUserViewModel>();

        public List<ShareProjectUserViewModel> Users { get; set; } = new List<ShareProjectUserViewModel>();
    }
}