﻿using Common.Entities;

namespace ProjectManagement.ViewModels.Users
{
    public class IndexViewModel
    {
        public List<IndexUserViewModel> Users { get; set; } = new List<IndexUserViewModel>();
    }
}
