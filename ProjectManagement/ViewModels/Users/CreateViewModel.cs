﻿using ProjectManagement.Utilities;
using ProjectManagement.ValidationAttributes;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ProjectManagement.ViewModels.Users
{
    public class CreateViewModel
    {
        [DisplayName("Username: ")]
        [Required(ErrorMessage = "This field is required!")]
        [MinLength(3, ErrorMessage = $"Username must be at least 3 characters long!")]
        [AllowedCharacters(Constants.ALLOWED_USERNAME_CHARS_REGEX,
            ErrorMessage = Messages.USERNAME_HAS_INVALID_CHARS)]
        public string Username { get; set; }

        [DisplayName("Password: ")]
        [Required(ErrorMessage = "This field is required!")]
        [MinLength(8, ErrorMessage = $"Username must be at least 8 characters long!")]
        public string Password { get; set; }

        [DisplayName("First name: ")]
        [Required(ErrorMessage = "This field is required!")]
        public string FirstName { get; set; }

        [DisplayName("Last name: ")]
        [Required(ErrorMessage = "This field is required!")]
        public string LastName { get; set; }
    }
}
