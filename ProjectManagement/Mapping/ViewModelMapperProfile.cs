﻿using AutoMapper;
using Common.Dtos;
using Common.Entities;
using ProjectManagement.Extensions;
using ViewModels = ProjectManagement.ViewModels;

namespace Common.Mapping
{
    public class ViewModelMapperProfile : Profile
    {
        public ViewModelMapperProfile()
        {
            CreateMap<User, ViewModels.Users.CreateViewModel>().ReverseMap();
            CreateMap<User, ViewModels.Users.EditViewModel>()
                .ForMember(
                    vm => vm.DbUsername,
                    options => options.MapFrom(u => u.Username)
                ).ReverseMap();
            CreateMap<User, ViewModels.Users.IndexUserViewModel>().ReverseMap();
            CreateMap<User, ViewModels.Projects.ShareProjectUserViewModel>().ReverseMap();
            CreateMap<User, ViewModels.Tasks.CreateTaskUserViewModel>().ReverseMap();

            CreateMap<Project, ViewModels.Projects.CreateViewModel>().ReverseMap();
            CreateMap<Project, ViewModels.Projects.EditViewModel>().ReverseMap();
            CreateMap<Project, ViewModels.Projects.IndexProjectViewModel>()
                .ForMember(
                    vm => vm.Description, 
                    options => options.MapFrom(p => p.Description.Truncate(100, "..."))
                ).ReverseMap();

            CreateMap<Entities.Task, ViewModels.Tasks.CreateViewModel>();
                //.ForMember(vm => vm.ProjectTitle, options => options.Ignore()).ReverseMap();
            CreateMap<ViewModels.Tasks.CreateViewModel, Entities.Task>()
                .ForSourceMember(vm => vm.ProjectTitle, options => options.DoNotValidate());
            CreateMap<Entities.Task, ViewModels.Tasks.EditViewModel>().ReverseMap();
            CreateMap<Entities.Task, ViewModels.Tasks.IndexTaskViewModel>().ReverseMap();


            CreateMap<UserDto, ViewModels.Users.CreateViewModel>().ReverseMap();
            CreateMap<UserDto, ViewModels.Users.EditViewModel>()
                .ForMember(
                    vm => vm.DbUsername,
                    options => options.MapFrom(u => u.Username)
                ).ReverseMap();
            CreateMap<UserDto, ViewModels.Users.IndexUserViewModel>().ReverseMap();
            CreateMap<UserDto, ViewModels.Projects.ShareProjectUserViewModel>().ReverseMap();
            CreateMap<UserDto, ViewModels.Tasks.CreateTaskUserViewModel>().ReverseMap();

            CreateMap<ProjectDto, ViewModels.Projects.CreateViewModel>().ReverseMap();
            CreateMap<ProjectDto, ViewModels.Projects.EditViewModel>().ReverseMap();
            CreateMap<ProjectDto, ViewModels.Projects.IndexProjectViewModel>()
                .ForMember(
                    vm => vm.Description,
                    options => options.MapFrom(p => p.Description.Truncate(100, "..."))
                ).ReverseMap();

            CreateMap<TaskDto, ViewModels.Tasks.CreateViewModel>().ReverseMap();
            CreateMap<TaskDto, ViewModels.Tasks.EditViewModel>().ReverseMap();
            CreateMap<TaskDto, ViewModels.Tasks.IndexTaskViewModel>().ReverseMap();

        }
    }
}
