﻿using System.Text.Json;

namespace ProjectManagement.Extensions
{
    public static class SessionExtensions
    {
        public static T? GetObject<T>(this ISession session, string key) where T : class
        {
            if (session is null) throw new ArgumentNullException(nameof(session));

            if (key is null) throw new ArgumentNullException(nameof(key));

            var valueAsString = session.GetString(key);
            if (String.IsNullOrWhiteSpace(valueAsString)) return null;

            return JsonSerializer.Deserialize<T>(valueAsString);
        }

        public static void SetObject<T>(this ISession session, string key, T value) where T : class
        {
            if (session is null) throw new ArgumentNullException(nameof(session));

            if (key is null) throw new ArgumentNullException(nameof(key));

            if (value is null)
            {
                session.Remove(key);
                return;
            }

            session.SetString(key, JsonSerializer.Serialize(value));
        }
    }
}
