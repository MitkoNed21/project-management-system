﻿namespace ProjectManagement.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Truncated a string to a specific length, adding a string to the end.
        /// If the string is shorter than <paramref name="length"/>, truncation is not applied.
        /// </summary>
        /// <param name="s">The string to truncate</param>
        /// <param name="length">The length of the truncated string</param>
        /// <param name="truncationEnd">The string that should be added to the end of the truncated string.</param>
        /// <returns>The string, truncated to be <paramref name="length"/> characters long,
        /// with <paramref name="truncationEnd"/> appended to the end.</returns>
        /// <exception cref="ArgumentNullException">If the input string is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="length"/> is less than 0.</exception>
        public static string Truncate(this string s, int length, string truncationEnd = "...")
        {
            if (s is null)
            {
                throw new ArgumentNullException(nameof(s));
            }

            if (length < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(length), "Length should be a non-negative number!");
            }

            if (length >= s.Length) return s;

            if (truncationEnd is null) truncationEnd = "";

            var truncatedString = s.Substring(0, length);
            return truncatedString += truncationEnd;
        }
    }
}
