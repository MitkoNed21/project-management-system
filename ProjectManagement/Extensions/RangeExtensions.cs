﻿namespace ProjectManagement.Extensions
{
    public static class RangeExtensions
    {
        /// <summary>
        /// Converts a range to an array containing all elements from the range,
        /// including the starting and ending elements.
        /// </summary>
        /// <param name="range">Range to convert</param>
        /// <returns>Array containing all elements from <paramref name="range"/>, 
        /// including the first and last ones.</returns>
        public static int[] ToArray(this Range range)
        {
            // if range is 5..7 -> values are 5, 6, 7; length should be 7 - 5 = 2 + 1 = 3
            var result = new int[range.End.Value - range.Start.Value + 1];

            var value = range.Start.Value;
            for (var i = 0; i < result.Length; i++)
            {
                result[i] = value;
                value++;
            }

            return result;
        }
    }
}
