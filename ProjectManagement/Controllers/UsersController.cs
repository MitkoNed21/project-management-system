﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ProjectManagement.ActionFilters;
using Common.Entities;
using Common.Repositories;
using ProjectManagement.Utilities;
using ProjectManagement.ViewModels.Users;
using AutoMapper;
using Common.Mapping;
using Common.Repositories.Contracts;

namespace ProjectManagement.Controllers
{
    [AuthenticationActionFilter]
    public class UsersController : Controller
    {
        PasswordHasher<User> passwordHasher;
        private readonly IUserRepository userRepository;
        IMapper mapper;

        public UsersController(
            IUserRepository userRepository,
            IMapper mapper
        )
        {
            this.passwordHasher = new PasswordHasher<User>();
            this.userRepository = userRepository;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var model = new IndexViewModel();
            
            model.Users = mapper.ProjectTo<IndexUserViewModel>(
                userRepository.GetAllUsers().AsQueryable()
            ).ToList();

            return PartialView(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View(new CreateViewModel());
        }

        [HttpPost]
        public IActionResult Create(CreateViewModel viewModel)
        {
            if (viewModel is null) return PartialView();

            if (!ModelState.IsValid) return PartialView(viewModel);

            if (context.Users.Any(u => u.Username == viewModel.Username))
            {
                ModelState.AddModelError(
                    Constants.USER_CREATION_ERROR_STRING_KEY,
                    Messages.USERNAME_ALREADY_EXISTS
                );
                return View(viewModel);
            }

            var user = mapper.Map<User>(viewModel);

            var hashedPassword = passwordHasher.HashPassword(user, viewModel.Password);
            user.Password = hashedPassword;

            context.Users.Add(user);
            context.SaveChanges();

            return Redirect(nameof(Index));
        }

        [HttpGet]
        public IActionResult Edit(int userId)
        {
            var user = context.Users.FirstOrDefault(u => u.Id == userId);

            if (user is null) return Redirect(nameof(Index));

            var model = mapper.Map<EditViewModel>(user);

            model.DbUsername = user.Username;

            // Start tracking currently edited user id.
            HttpContext.Session.SetString(Constants.CURRENTLY_EDITED_USER_STRING_KEY, userId.ToString());

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(EditViewModel viewModel)
        {
            if (viewModel is null) return PartialView();

            if (!ModelState.IsValid) return PartialView(viewModel);

            // A user id was tracked when starting to edit. Retrieve it.
            if (!int.TryParse(HttpContext.Session.GetString(
                    Constants.CURRENTLY_EDITED_USER_STRING_KEY
                ), out int editedUserId))
            {
                //ModelState.AddModelError(
                //    Constants.USER_EDITING_ERROR_STRING_KEY,
                //    Messages.EDITED_USER_INVALID_ID
                //);

                return Redirect(nameof(Index));
            }

            if (context.Users.Any(u =>
                    u.Id != editedUserId &&
                    u.Username == viewModel.Username
                ))
            {
                ModelState.AddModelError(
                    Constants.USER_EDITING_ERROR_STRING_KEY,
                    Messages.USERNAME_ALREADY_EXISTS
                );
                return View(viewModel);
            }

            var user = context.Users.FirstOrDefault(u => u.Id == editedUserId);

            if (user is null) return Redirect(nameof(Index));

            mapper.Map(viewModel, user);

            var hashedPassword = passwordHasher.HashPassword(user, viewModel.Password);
            user.Password = hashedPassword;

            context.Users.Update(user);
            context.SaveChanges();

            // User editing done, remove id from being tracked.
            HttpContext.Session.Remove(Constants.CURRENTLY_EDITED_USER_STRING_KEY);

            return Redirect(nameof(Index));
        }

        [HttpGet]
        public IActionResult Delete(int userId)
        {
            if (context.Users.Any(u => u.Id == userId))
            {
                var user = new User() { Id = userId };

                context.Users.Remove(user);
                context.SaveChanges();
            }

            return Redirect(nameof(Index));
        }
    }
}
