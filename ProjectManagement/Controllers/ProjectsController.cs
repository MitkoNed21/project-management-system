﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjectManagement.ActionFilters;
using Common.Entities;
using ProjectManagement.Extensions;
using Common.Repositories;
using ProjectManagement.Utilities;
using ProjectManagement.ViewModels.Projects;
using AutoMapper;
using Common.Mapping;

namespace ProjectManagement.Controllers
{
    [AuthenticationActionFilter]
    public class ProjectsController : Controller
    {
        ProjectManagementSystemDbContext context;
        private IMapper mapper;

        public ProjectsController(
            ProjectManagementSystemDbContext context,
            IMapper mapper
        )
        {
            this.context = context;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var model = new IndexViewModel();

            var loggedUser = Miscellaneous.GetLoggedUser(HttpContext.Session)!;

            var projects = context.Projects.Where(p =>
                p.OwnerId == loggedUser.Id ||
                p.SharedUsers.Select(su => su.Id).Contains(loggedUser.Id)
            );

            model.Projects = mapper.ProjectTo<IndexProjectViewModel>(
                projects
            ).ToList();

            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View(new CreateViewModel());
        }

        [HttpPost]
        public IActionResult Create(CreateViewModel viewModel)
        {
            if (viewModel is null) return PartialView();

            if (!ModelState.IsValid) return PartialView(viewModel);

            var project = mapper.Map<Project>(viewModel);
            project.OwnerId = Miscellaneous.GetLoggedUser(HttpContext.Session)!.Id;

            context.Projects.Add(project);
            context.SaveChanges();

            return Redirect(nameof(Index));
        }

        [HttpGet]
        public IActionResult Edit(int projectId)
        {
            var project = context.Projects.FirstOrDefault(p => p.Id == projectId);
            var loggedUser = Miscellaneous.GetLoggedUser(HttpContext.Session)!;

            if (project is null || project.OwnerId != loggedUser.Id) return Redirect(nameof(Index));

            var model = mapper.Map<EditViewModel>(project);

            // Start tracking currently edited project id.
            HttpContext.Session.SetString(Constants.CURRENTLY_EDITED_PROJECT_STRING_KEY, projectId.ToString());

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(EditViewModel viewModel)
        {
            if (viewModel is null) return PartialView();

            if (!ModelState.IsValid) return PartialView(viewModel);

            // A project id was tracked when starting to edit. Retrieve it.
            if (!int.TryParse(HttpContext.Session.GetString(
                    Constants.CURRENTLY_EDITED_PROJECT_STRING_KEY
                ), out int editedProjectId))
            {
                ModelState.AddModelError(
                    Constants.PROJECT_EDITING_ERROR_STRING_KEY,
                    Messages.EDITED_PROJECT_INVALID_ID
                );
                return View(viewModel);
            }

            var project = context.Projects.FirstOrDefault(p => p.Id == editedProjectId);

            if (project is null) return Redirect(nameof(Index));

            var loggedUser = Miscellaneous.GetLoggedUser(HttpContext.Session)!;

            if (project.OwnerId != loggedUser.Id)
            {
                ModelState.AddModelError(
                    Constants.PROJECT_EDITING_ERROR_STRING_KEY,
                    Messages.OWNER_IMPERSONATION_DETECTED
                );
                return View(viewModel);
            }

            mapper.Map(viewModel, project);

            context.Projects.Update(project);
            context.SaveChanges();

            // Project editing done, remove id from being tracked.
            HttpContext.Session.Remove(Constants.CURRENTLY_EDITED_PROJECT_STRING_KEY);

            return Redirect(nameof(Index));
        }

        [HttpGet]
        public IActionResult Delete(int projectId)
        {
            var project = context.Projects.FirstOrDefault(u => u.Id == projectId);

            if (project is not null)
            {
                var loggedUser = Miscellaneous.GetLoggedUser(HttpContext.Session)!;

                if (project.OwnerId == loggedUser.Id)
                {
                    context.Projects.Remove(project);
                    context.SaveChanges();
                }
            }

            return Redirect(nameof(Index));
        }

        [HttpGet]
        public IActionResult Share(int projectId)
        {
            var project = context.Projects
                .FirstOrDefault(p => p.Id == projectId);
            var loggedUser = Miscellaneous.GetLoggedUser(HttpContext.Session)!;

            if (project is null || project.OwnerId != loggedUser.Id) return Redirect(nameof(Index));

            var model = new ShareViewModel();
            model.SharedUsers = mapper.ProjectTo<ShareProjectUserViewModel>(
                project.SharedUsers.AsQueryable()
            ).ToList();

            var sharedUsersIds = project.SharedUsers.Select(su => su.Id).ToList();
            // Treat project as if shared with current user.
            sharedUsersIds.Add(loggedUser.Id);
            model.Users = mapper.ProjectTo<ShareProjectUserViewModel>(
                context.Users.Where(u => !sharedUsersIds.Contains(u.Id))
            ).ToList();
                
            model.ProjectId = projectId;
            model.ProjectName = project.Title;

            // Start tracking currently shared project id.
            HttpContext.Session.SetString(Constants.CURRENTLY_SHARED_PROJECT_STRING_KEY, projectId.ToString());

            return View(model);
        }

        [HttpPost]
        public IActionResult Share(ShareViewModel viewModel)
        {
            if (viewModel is null) return Redirect(nameof(Index));

            var loggedUser = Miscellaneous.GetLoggedUser(HttpContext.Session)!;

            // A project id was tracked when starting to share. Retrieve it.
            if (!int.TryParse(HttpContext.Session.GetString(
                    Constants.CURRENTLY_SHARED_PROJECT_STRING_KEY
                ), out int sharedProjectId))
            {
                return Redirect(nameof(Index));
            }

            var project = context.Projects
                .FirstOrDefault(p => p.Id == sharedProjectId);

            if (project is null) return Redirect(nameof(Index));

            if (project.OwnerId != loggedUser.Id)
            {
                ModelState.AddModelError(
                    Constants.PROJECT_EDITING_ERROR_STRING_KEY,
                    Messages.OWNER_IMPERSONATION_DETECTED
                );
            }

            ModelState.Remove(nameof(ShareViewModel.ProjectName));
            if (!ModelState.IsValid)
            {
                viewModel.ProjectName = project.Title;

                viewModel.SharedUsers = mapper.ProjectTo<ShareProjectUserViewModel>(
                    project.SharedUsers.AsQueryable()
                ).ToList();

                var sharedUsersIds = project.SharedUsers.Select(su => su.Id).ToList();
                // Treat project as if shared with current user.
                sharedUsersIds.Add(loggedUser.Id);
                viewModel.Users = mapper.ProjectTo<ShareProjectUserViewModel>(
                    context.Users.Where(u => !sharedUsersIds.Contains(u.Id))
                ).ToList();

                viewModel.Users.RemoveAll(u => u.Id == loggedUser.Id);
                return View(viewModel);
            }

            var sharedUserIds = viewModel.UserIdsToShareWith;

            foreach (var userId in sharedUserIds)
            {
                var sharedUser = context.Users.FirstOrDefault(u => u.Id == userId);

                if (sharedUser is not null)
                {
                    project.SharedUsers.Add(sharedUser);
                }
            }

            context.Projects.Update(project);
            context.SaveChanges();

            // Project editing done, remove id from being tracked.
            HttpContext.Session.Remove(Constants.CURRENTLY_EDITED_PROJECT_STRING_KEY);

            return Redirect(nameof(Index));
        }
    }
}
