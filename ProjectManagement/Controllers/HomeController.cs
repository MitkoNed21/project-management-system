﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Common.Entities;
using ProjectManagement.Extensions;
using Common.Repositories;
using ProjectManagement.ViewModels.Home;
using ProjectManagement.Utilities;
using ProjectManagement.ActionFilters;
using AutoMapper;
using Common.Dtos;

namespace ProjectManagement.Controllers
{
    public class HomeController : Controller
    {
        ProjectManagementSystemDbContext context;
        IMapper mapper;

        public HomeController(
            ProjectManagementSystemDbContext context,
            IMapper mapper
        )
        {
            this.context = context;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(LoginViewModel viewModel)
        {
            if (viewModel is null) return PartialView();

            if (!ModelState.IsValid) return PartialView(viewModel);

            var user = context.Users.FirstOrDefault(u => u.Username == viewModel.Username);

            if (user is null)
            {
                ModelState.AddModelError(
                    Constants.AUTHENTICATION_ERROR_STRING_KEY,
                    Messages.USERNAME_DOES_NOT_EXIST
                );
                return PartialView(viewModel);
            }

            var hasher = new PasswordHasher<User>();
            var passwordCheckResult = hasher.VerifyHashedPassword(user, user.Password, viewModel.Password);

            if (passwordCheckResult == PasswordVerificationResult.Failed)
            {
                ModelState.AddModelError(
                    Constants.AUTHENTICATION_ERROR_STRING_KEY,
                    Messages.WRONG_PASSWORD
                );
                return PartialView(viewModel);
            }

            var userDto = mapper.Map<UserDto>(user);
            HttpContext.Session.SetObject(Constants.LOGGED_USER_SESSION_KEY, userDto);

            return Redirect(nameof(Index));
        }

        [HttpGet]
        [AuthenticationActionFilter]
        public IActionResult Logout()
        {
            HttpContext.Session.Remove(Constants.LOGGED_USER_SESSION_KEY);

            return Redirect(nameof(Index));
        }
    }
}
