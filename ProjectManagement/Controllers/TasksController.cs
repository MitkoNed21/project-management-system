﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProjectManagement.ActionFilters;
using Common.Entities;
using ProjectManagement.Extensions;
using Common.Repositories;
using ProjectManagement.Utilities;
using ProjectManagement.ViewModels.Tasks;
using AutoMapper;

namespace ProjectManagement.Controllers
{
    [AuthenticationActionFilter]
    public class TasksController : Controller
    {
        ProjectManagementSystemDbContext context;
        IMapper mapper;

        public TasksController(
            ProjectManagementSystemDbContext context,
            IMapper mapper
        )
        {
            this.context = context;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index(int projectId)
        {
            //new SelectList(null, "Key", "Value");
            //TODO: Track project
            var loggedUser = Miscellaneous.GetLoggedUser(HttpContext.Session)!;

            var project = context.Projects.FirstOrDefault(p => p.Id == projectId);

            if (project is null || (project.OwnerId != loggedUser.Id &&
                !project.SharedUsers.Select(su => su.Id).Contains(loggedUser.Id)))
            {
                var projectsControllerName = Miscellaneous.GetControllerName<ProjectsController>();
                return Redirect($"/{projectsControllerName}");
            }

            var model = new IndexViewModel();
            model.Tasks = mapper.ProjectTo<IndexTaskViewModel>(
                context.Tasks.Where(t => t.ProjectId == projectId)
            ).ToList();

            model.ProjectId = projectId;
            model.ProjectName = project.Title;

            return View(model);
        }

        [HttpGet]
        public IActionResult Create(int projectId)
        {
            var project = context.Projects.FirstOrDefault(p => p.Id == projectId);
            var loggedUser = Miscellaneous.GetLoggedUser(HttpContext.Session)!;

            if (project is null || (project.OwnerId != loggedUser.Id &&
                !project.SharedUsers.Select(su => su.Id).Contains(loggedUser.Id)))
            {
                var projectsControllerName = Miscellaneous.GetControllerName<ProjectsController>();
                return Redirect($"/{projectsControllerName}");
            }

            var viewModel = new CreateViewModel();
            viewModel.ProjectId = projectId;
            viewModel.ProjectTitle = project.Title;
            viewModel.AssigneeId = loggedUser.Id;
            viewModel.Users = mapper.ProjectTo<CreateTaskUserViewModel>(context.Users).ToList();

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Create(CreateViewModel viewModel)
        {
            if (viewModel is null) return PartialView();

            var project = context.Projects.FirstOrDefault(p => p.Id == viewModel.ProjectId);
            if (project is null)
            {
                var projectsControllerName = Miscellaneous.GetControllerName<ProjectsController>();
                return Redirect($"/{projectsControllerName}");
            }

            var loggedUser = Miscellaneous.GetLoggedUser(HttpContext.Session)!;

            // For some reason the framework makes these invalid,
            // even though they are not marked with a [Required] attribute.
            /**
             * Adapted from:
             * https://stackoverflow.com/q/31519223
             * answer by Slicksim
             * https://stackoverflow.com/a/31520640
             */
            ModelState.Remove(nameof(CreateViewModel.Description));
            ModelState.Remove(nameof(CreateViewModel.ProjectTitle));
            ModelState.Remove(nameof(CreateViewModel.Users));
            if (!ModelState.IsValid)
            {
                if (project.OwnerId != loggedUser.Id &&
                    !project.SharedUsers.Select(su => su.Id).Contains(loggedUser.Id))
                {
                    var projectsControllerName = Miscellaneous.GetControllerName<ProjectsController>();
                    return Redirect($"/{projectsControllerName}");
                }

                viewModel.ProjectTitle = project.Title;
                viewModel.Users = mapper.ProjectTo<CreateTaskUserViewModel>(context.Users).ToList();
                return View(viewModel);
            }

            if (project.OwnerId != loggedUser.Id &&
                !project.SharedUsers.Select(su => su.Id).Contains(loggedUser.Id))
            {
                ModelState.AddModelError(
                    Constants.TASK_CREATION_ERROR_STRING_KEY,
                    Messages.OWNER_IMPERSONATION_DETECTED
                );
                return View(viewModel);
            }

            var task = mapper.Map<Common.Entities.Task>(viewModel);
            task.OwnerId = loggedUser.Id;

            context.Tasks.Add(task);
            context.SaveChanges();

            return RedirectToAction(nameof(Index), new { projectId = project.Id });
        }
    }
}
