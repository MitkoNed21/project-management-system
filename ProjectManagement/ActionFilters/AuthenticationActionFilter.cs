﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Common.Entities;
using ProjectManagement.Extensions;
using ProjectManagement.Utilities;

namespace ProjectManagement.ActionFilters
{
    public class AuthenticationActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            //TODO: Convert To Dto
            if (context.HttpContext.Session.GetObject<User>(Constants.LOGGED_USER_SESSION_KEY) is null)
            {
                context.Result = new RedirectResult("/Home/Index");
            }

            base.OnActionExecuting(context);
        }
    }
}
