﻿namespace ProjectManagement.Utilities
{
    public static class Constants
    {
        public const string LOGGED_USER_SESSION_KEY = "loggedUser";
        public const string AUTHENTICATION_ERROR_STRING_KEY = "authenticationError";
        public const string ALLOWED_USERNAME_CHARS_REGEX = @"^[A-Za-z0-9_]+$";

        public const string CURRENTLY_EDITED_USER_STRING_KEY = "editedUserId";
        public const string CURRENTLY_EDITED_PROJECT_STRING_KEY = "editedProjectId";
        public const string CURRENTLY_SHARED_PROJECT_STRING_KEY = "sharedProjectId";
        public const string CURRENTLY_MANAGED_TASKS_PROJECT_STRING_KEY = "managedTasksProjectId";

        public const string USER_CREATION_ERROR_STRING_KEY = "userCreationError";
        public const string USER_EDITING_ERROR_STRING_KEY = "userEditingError";

        public const string PROJECT_CREATION_ERROR_STRING_KEY = "projectCreationError";
        public const string PROJECT_EDITING_ERROR_STRING_KEY = "projectEditingError";
        public const string PROJECT_SHARING_ERROR_STRING_KEY = "projectSharingError";

        public const string TASK_CREATION_ERROR_STRING_KEY = "taskCreationError";
        public const string TASK_EDITING_ERROR_STRING_KEY = "taskEditingError";
    }
}
