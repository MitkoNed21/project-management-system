﻿namespace ProjectManagement.Utilities
{
    public static class Messages
    {
        public const string USERNAME_DOES_NOT_EXIST = "A user with this username does not exits.";
        public const string WRONG_PASSWORD = "Incorrect password. Please, try again.";

        //public const string USERNAME_HAS_INVALID_CHARS = "The username has invalid characters. Valid characters are uppercase and lowercase letters, digits, and underscore.";
        public const string USERNAME_HAS_INVALID_CHARS = "The username has invalid characters. Valid characters are a-z, A-Z, 0-9, _";
        public const string USERNAME_ALREADY_EXISTS = "A user with this username already exists.";

        public const string EDITED_USER_INVALID_ID = "The user being edited has an invalid ID!";

        public const string EDITED_PROJECT_INVALID_ID = "The project being edited has an invalid ID!";

        public const string OWNER_IMPERSONATION_DETECTED = "An owner impersonation attempt was detected!";
    }
}
