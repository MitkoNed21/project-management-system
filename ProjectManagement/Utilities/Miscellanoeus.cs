﻿using Common.Dtos;
using Microsoft.AspNetCore.Mvc;
using ProjectManagement.Extensions;

namespace ProjectManagement.Utilities
{
    public class Miscellaneous
    {
        /// <summary>
        /// For a controller named "SampleController", removes the "Controller" part of the name,
        /// and returns the result.
        /// </summary>
        /// <typeparam name="T">A <see cref="Microsoft.AspNetCore.Mvc"/> controller class.</typeparam>
        /// <returns>For a controller named "SampleController", returns "Sample".</returns>
        public static string GetControllerName<T>() where T : Controller
        {
            var name = typeof(T).Name;
            // Gets substring from 0 to "Controller".Length where controllers have a name like:
            // "SampleController". For a controller named "SampleController", result will be "Sample".
            return name[0..^"Controller".Length];
        }

        /// <summary>
        /// Throws a <see cref="System.NotImplementedException"/> with provided <paramref name="message"/>.
        /// </summary>
        /// <param name="message">The message to provide, can be null</param>
        /// <exception cref="NotImplementedException"></exception>
        public static void TODO(string? message)
        {
            throw new NotImplementedException(message);
        }

        /// <summary>
        ///     Get the logged in user from a <see cref="Microsoft.AspNetCore.Http.ISession"/>.
        /// </summary>
        /// <param name="session">An <see cref="Microsoft.AspNetCore.Http.ISession"/>
        ///     to get the user from.</param>
        /// <returns>A <see cref="Common.Dtos.UserDto"/> of the logged in user,
        ///     or <see cref="null"/> if there isn't one.</returns>
        public static UserDto? GetLoggedUser(ISession session)
        {
            return session.GetObject<UserDto>(Constants.LOGGED_USER_SESSION_KEY);
        }
    }
}
